using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Runtime.InteropServices;

namespace TicTacToe
{

	public class frmTicTacToe : System.Windows.Forms.Form
	{
		#region�Form Initialization 

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.PictureBox pic23;
		private System.Windows.Forms.PictureBox pic22;
		private System.Windows.Forms.PictureBox pic33;
		private System.Windows.Forms.PictureBox pic32;
		private System.Windows.Forms.PictureBox pic31;
		private System.Windows.Forms.PictureBox pic11;
		private System.Windows.Forms.PictureBox pic12;
		private System.Windows.Forms.PictureBox pic13;
		private System.Windows.Forms.PictureBox pic21;
		private System.Windows.Forms.PictureBox picWinner;
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.StatusBar stbMessage;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem menuItem3;
		private System.Windows.Forms.MenuItem menuItem5;
		private System.Windows.Forms.MenuItem mnNew;
		private System.Windows.Forms.MenuItem mnExit;
		private System.Windows.Forms.MenuItem mnOnePlayer;
		private System.Windows.Forms.MenuItem mnTwoPlayers;
		private System.Windows.Forms.MenuItem menuItem2;
		private System.Windows.Forms.MenuItem mnCPUX;
		private System.Windows.Forms.MenuItem mnCPUO;
		private System.Windows.Forms.MenuItem mnDifficultHard;
		private System.Windows.Forms.MenuItem mnDifficultEasy;
		private System.Windows.Forms.MenuItem mnDificuldade;
		private System.Windows.Forms.MenuItem mnDifficultAverage;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label lbScoreBall;
		private System.Windows.Forms.Label lbScoreCross;
		private System.Windows.Forms.Label lbScoreDraw;
		private System.Windows.Forms.Label lbMaches;
		private System.Windows.Forms.MenuItem menuItem4;
		private System.Windows.Forms.MenuItem mnResetScore;
		private System.Windows.Forms.ImageList ilBolaXis;
		private System.Windows.Forms.GroupBox gbScore;
		private MenuItem menuItem6;
		private System.ComponentModel.IContainer components;

		public frmTicTacToe()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#endregion

		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTicTacToe));
            this.pic11 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pic12 = new System.Windows.Forms.PictureBox();
            this.pic13 = new System.Windows.Forms.PictureBox();
            this.pic23 = new System.Windows.Forms.PictureBox();
            this.pic22 = new System.Windows.Forms.PictureBox();
            this.pic21 = new System.Windows.Forms.PictureBox();
            this.pic33 = new System.Windows.Forms.PictureBox();
            this.pic32 = new System.Windows.Forms.PictureBox();
            this.pic31 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.picWinner = new System.Windows.Forms.PictureBox();
            this.mainMenu1 = new System.Windows.Forms.MainMenu(this.components);
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.mnNew = new System.Windows.Forms.MenuItem();
            this.menuItem3 = new System.Windows.Forms.MenuItem();
            this.mnResetScore = new System.Windows.Forms.MenuItem();
            this.menuItem4 = new System.Windows.Forms.MenuItem();
            this.mnExit = new System.Windows.Forms.MenuItem();
            this.menuItem5 = new System.Windows.Forms.MenuItem();
            this.mnOnePlayer = new System.Windows.Forms.MenuItem();
            this.mnTwoPlayers = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.mnCPUX = new System.Windows.Forms.MenuItem();
            this.mnCPUO = new System.Windows.Forms.MenuItem();
            this.mnDificuldade = new System.Windows.Forms.MenuItem();
            this.mnDifficultEasy = new System.Windows.Forms.MenuItem();
            this.mnDifficultAverage = new System.Windows.Forms.MenuItem();
            this.mnDifficultHard = new System.Windows.Forms.MenuItem();
            this.menuItem6 = new System.Windows.Forms.MenuItem();
            this.stbMessage = new System.Windows.Forms.StatusBar();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lbScoreBall = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lbScoreCross = new System.Windows.Forms.Label();
            this.lbScoreDraw = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lbMaches = new System.Windows.Forms.Label();
            this.gbScore = new System.Windows.Forms.GroupBox();
            this.ilBolaXis = new System.Windows.Forms.ImageList(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pic11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picWinner)).BeginInit();
            this.gbScore.SuspendLayout();
            this.SuspendLayout();
            // 
            // pic11
            // 
            this.pic11.Location = new System.Drawing.Point(41, 31);
            this.pic11.Name = "pic11";
            this.pic11.Size = new System.Drawing.Size(74, 65);
            this.pic11.TabIndex = 0;
            this.pic11.TabStop = false;
            this.pic11.Click += new System.EventHandler(this.pic1_Click);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(118, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(9, 244);
            this.label1.TabIndex = 1;
            // 
            // pic12
            // 
            this.pic12.Location = new System.Drawing.Point(131, 31);
            this.pic12.Name = "pic12";
            this.pic12.Size = new System.Drawing.Size(74, 65);
            this.pic12.TabIndex = 2;
            this.pic12.TabStop = false;
            this.pic12.Click += new System.EventHandler(this.pic1_Click);
            // 
            // pic13
            // 
            this.pic13.Location = new System.Drawing.Point(224, 31);
            this.pic13.Name = "pic13";
            this.pic13.Size = new System.Drawing.Size(75, 65);
            this.pic13.TabIndex = 3;
            this.pic13.TabStop = false;
            this.pic13.Click += new System.EventHandler(this.pic1_Click);
            // 
            // pic23
            // 
            this.pic23.Location = new System.Drawing.Point(224, 114);
            this.pic23.Name = "pic23";
            this.pic23.Size = new System.Drawing.Size(75, 65);
            this.pic23.TabIndex = 6;
            this.pic23.TabStop = false;
            this.pic23.Click += new System.EventHandler(this.pic1_Click);
            // 
            // pic22
            // 
            this.pic22.Location = new System.Drawing.Point(132, 112);
            this.pic22.Name = "pic22";
            this.pic22.Size = new System.Drawing.Size(74, 65);
            this.pic22.TabIndex = 5;
            this.pic22.TabStop = false;
            this.pic22.Click += new System.EventHandler(this.pic1_Click);
            // 
            // pic21
            // 
            this.pic21.Location = new System.Drawing.Point(41, 113);
            this.pic21.Name = "pic21";
            this.pic21.Size = new System.Drawing.Size(74, 65);
            this.pic21.TabIndex = 4;
            this.pic21.TabStop = false;
            this.pic21.Click += new System.EventHandler(this.pic1_Click);
            // 
            // pic33
            // 
            this.pic33.Location = new System.Drawing.Point(224, 196);
            this.pic33.Name = "pic33";
            this.pic33.Size = new System.Drawing.Size(75, 65);
            this.pic33.TabIndex = 9;
            this.pic33.TabStop = false;
            this.pic33.Click += new System.EventHandler(this.pic1_Click);
            // 
            // pic32
            // 
            this.pic32.Location = new System.Drawing.Point(132, 196);
            this.pic32.Name = "pic32";
            this.pic32.Size = new System.Drawing.Size(74, 65);
            this.pic32.TabIndex = 8;
            this.pic32.TabStop = false;
            this.pic32.Click += new System.EventHandler(this.pic1_Click);
            // 
            // pic31
            // 
            this.pic31.Location = new System.Drawing.Point(41, 196);
            this.pic31.Name = "pic31";
            this.pic31.Size = new System.Drawing.Size(74, 65);
            this.pic31.TabIndex = 7;
            this.pic31.TabStop = false;
            this.pic31.Click += new System.EventHandler(this.pic1_Click);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(210, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(10, 246);
            this.label2.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(25, 98);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(286, 13);
            this.label3.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(26, 179);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(286, 13);
            this.label4.TabIndex = 12;
            // 
            // picWinner
            // 
            this.picWinner.Location = new System.Drawing.Point(0, 0);
            this.picWinner.Name = "picWinner";
            this.picWinner.Size = new System.Drawing.Size(344, 301);
            this.picWinner.TabIndex = 13;
            this.picWinner.TabStop = false;
            this.picWinner.Visible = false;
            this.picWinner.Paint += new System.Windows.Forms.PaintEventHandler(this.picWinner_Paint);
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem1,
            this.menuItem5,
            this.mnDificuldade,
            this.menuItem6});
            // 
            // menuItem1
            // 
            this.menuItem1.Index = 0;
            this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnNew,
            this.menuItem3,
            this.mnResetScore,
            this.menuItem4,
            this.mnExit});
            this.menuItem1.Text = "&Game";
            // 
            // mnNew
            // 
            this.mnNew.Index = 0;
            this.mnNew.Shortcut = System.Windows.Forms.Shortcut.F2;
            this.mnNew.Text = "&New";
            // 
            // menuItem3
            // 
            this.menuItem3.Index = 1;
            this.menuItem3.Text = "-";
            // 
            // mnResetScore
            // 
            this.mnResetScore.Index = 2;
            this.mnResetScore.Text = "&Reset score";
            this.mnResetScore.Click += new System.EventHandler(this.mnResetScore_Click);
            // 
            // menuItem4
            // 
            this.menuItem4.Index = 3;
            this.menuItem4.Text = "-";
            // 
            // mnExit
            // 
            this.mnExit.Index = 4;
            this.mnExit.Text = "&Exit";
            this.mnExit.Click += new System.EventHandler(this.mnExit_Click);
            // 
            // menuItem5
            // 
            this.menuItem5.Index = 1;
            this.menuItem5.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnOnePlayer,
            this.mnTwoPlayers,
            this.menuItem2,
            this.mnCPUX,
            this.mnCPUO});
            this.menuItem5.Text = "&Players";
            // 
            // mnOnePlayer
            // 
            this.mnOnePlayer.Checked = true;
            this.mnOnePlayer.Index = 0;
            this.mnOnePlayer.Text = "&One player";
            this.mnOnePlayer.Click += new System.EventHandler(this.mnOnePlayer_Click);
            // 
            // mnTwoPlayers
            // 
            this.mnTwoPlayers.Index = 1;
            this.mnTwoPlayers.Text = "&Two players";
            this.mnTwoPlayers.Click += new System.EventHandler(this.mnTwoPlayers_Click);
            // 
            // menuItem2
            // 
            this.menuItem2.Index = 2;
            this.menuItem2.Text = "-";
            // 
            // mnCPUX
            // 
            this.mnCPUX.Checked = true;
            this.mnCPUX.Index = 3;
            this.mnCPUX.Text = "CPU - &X";
            this.mnCPUX.Click += new System.EventHandler(this.mnCPUX_Click);
            // 
            // mnCPUO
            // 
            this.mnCPUO.Index = 4;
            this.mnCPUO.Text = "CPU - &O";
            this.mnCPUO.Click += new System.EventHandler(this.mnCPUO_Click);
            // 
            // mnDificuldade
            // 
            this.mnDificuldade.Index = 2;
            this.mnDificuldade.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnDifficultEasy,
            this.mnDifficultAverage,
            this.mnDifficultHard});
            this.mnDificuldade.Text = "&Difficulty";
            // 
            // mnDifficultEasy
            // 
            this.mnDifficultEasy.Index = 0;
            this.mnDifficultEasy.Text = "Easy";
            this.mnDifficultEasy.Click += new System.EventHandler(this.mnDifficultEasy_Click);
            // 
            // mnDifficultAverage
            // 
            this.mnDifficultAverage.Index = 1;
            this.mnDifficultAverage.Text = "Average";
            this.mnDifficultAverage.Click += new System.EventHandler(this.mnDifficultAverage_Click);
            // 
            // mnDifficultHard
            // 
            this.mnDifficultHard.Checked = true;
            this.mnDifficultHard.Index = 2;
            this.mnDifficultHard.Text = "Hard";
            this.mnDifficultHard.Click += new System.EventHandler(this.mnDifficultHard_Click);
            // 
            // menuItem6
            // 
            this.menuItem6.Index = 3;
            this.menuItem6.Text = "";
            // 
            // stbMessage
            // 
            this.stbMessage.Location = new System.Drawing.Point(0, 443);
            this.stbMessage.Name = "stbMessage";
            this.stbMessage.ShowPanels = true;
            this.stbMessage.Size = new System.Drawing.Size(357, 25);
            this.stbMessage.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(10, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 19);
            this.label5.TabIndex = 15;
            this.label5.Text = "Score:";
            // 
            // label6
            // 
            this.label6.ForeColor = System.Drawing.Color.Blue;
            this.label6.Location = new System.Drawing.Point(18, 48);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 27);
            this.label6.TabIndex = 16;
            this.label6.Text = "Ball:";
            // 
            // lbScoreBall
            // 
            this.lbScoreBall.ForeColor = System.Drawing.Color.Blue;
            this.lbScoreBall.Location = new System.Drawing.Point(60, 48);
            this.lbScoreBall.Name = "lbScoreBall";
            this.lbScoreBall.Size = new System.Drawing.Size(59, 27);
            this.lbScoreBall.TabIndex = 17;
            this.lbScoreBall.Text = "0";
            // 
            // label8
            // 
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(122, 48);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(46, 27);
            this.label8.TabIndex = 18;
            this.label8.Text = "Cross:";
            // 
            // lbScoreCross
            // 
            this.lbScoreCross.ForeColor = System.Drawing.Color.Red;
            this.lbScoreCross.Location = new System.Drawing.Point(167, 48);
            this.lbScoreCross.Name = "lbScoreCross";
            this.lbScoreCross.Size = new System.Drawing.Size(59, 27);
            this.lbScoreCross.TabIndex = 19;
            this.lbScoreCross.Text = "0";
            // 
            // lbScoreDraw
            // 
            this.lbScoreDraw.ForeColor = System.Drawing.Color.Green;
            this.lbScoreDraw.Location = new System.Drawing.Point(282, 48);
            this.lbScoreDraw.Name = "lbScoreDraw";
            this.lbScoreDraw.Size = new System.Drawing.Size(40, 27);
            this.lbScoreDraw.TabIndex = 21;
            this.lbScoreDraw.Text = "0";
            // 
            // label11
            // 
            this.label11.ForeColor = System.Drawing.Color.Green;
            this.label11.Location = new System.Drawing.Point(226, 48);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(54, 27);
            this.label11.TabIndex = 20;
            this.label11.Text = "Draw:";
            // 
            // lbMaches
            // 
            this.lbMaches.Location = new System.Drawing.Point(73, 16);
            this.lbMaches.Name = "lbMaches";
            this.lbMaches.Size = new System.Drawing.Size(120, 19);
            this.lbMaches.TabIndex = 22;
            this.lbMaches.Text = "0 matches";
            // 
            // gbScore
            // 
            this.gbScore.Controls.Add(this.label8);
            this.gbScore.Controls.Add(this.lbMaches);
            this.gbScore.Controls.Add(this.lbScoreCross);
            this.gbScore.Controls.Add(this.lbScoreDraw);
            this.gbScore.Controls.Add(this.label11);
            this.gbScore.Controls.Add(this.label5);
            this.gbScore.Controls.Add(this.label6);
            this.gbScore.Controls.Add(this.lbScoreBall);
            this.gbScore.Location = new System.Drawing.Point(8, 307);
            this.gbScore.Name = "gbScore";
            this.gbScore.Size = new System.Drawing.Size(338, 97);
            this.gbScore.TabIndex = 23;
            this.gbScore.TabStop = false;
            // 
            // ilBolaXis
            // 
            this.ilBolaXis.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilBolaXis.ImageStream")));
            this.ilBolaXis.TransparentColor = System.Drawing.Color.Transparent;
            this.ilBolaXis.Images.SetKeyName(0, "");
            this.ilBolaXis.Images.SetKeyName(1, "");
            // 
            // frmTicTacToe
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(6, 15);
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(357, 468);
            this.Controls.Add(this.gbScore);
            this.Controls.Add(this.stbMessage);
            this.Controls.Add(this.picWinner);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pic33);
            this.Controls.Add(this.pic32);
            this.Controls.Add(this.pic31);
            this.Controls.Add(this.pic23);
            this.Controls.Add(this.pic22);
            this.Controls.Add(this.pic21);
            this.Controls.Add(this.pic13);
            this.Controls.Add(this.pic12);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pic11);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Menu = this.mainMenu1;
            this.Name = "frmTicTacToe";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Improved Tic Tac Toe";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.frmTicTacToe_KeyUp);
            ((System.ComponentModel.ISupportInitialize)(this.pic11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picWinner)).EndInit();
            this.gbScore.ResumeLayout(false);
            this.ResumeLayout(false);

		}


		#region�Variables/API calls

		GameAI objGameAI;

		int wNumberPlayers=1;

		// It indicates if it was a client or server turned on for network play

		// SCORE

		int wScoreTotalMatches=0;
		int wScoreBall=0;
		int wScoreCross=0;
		int wScoreDraw=0;

		// The kind of line to be draw in case of a winner

		private enLineType wLineType=enLineType.Vertical;
		private int wLinePosition;


		// CPU AI difficulty

		public enDifficulty wDifficulty=enDifficulty.Hard;

		// Keyboard control

		public enum enControl
		{
			BallLeft=0,
			BallRight=1
		}
		private enControl wControl=enControl.BallRight;


		// Who plays, who won, the kind of player is CPU and human

		public enPlayType wTurn=enPlayType.Ball;
		public enPlayType wWinner=enPlayType.None;

		public enPlayType wTypeCPU=enPlayType.Cross;
		public enPlayType wTypePlayer=enPlayType.Ball;


		// Board is saved in a matrix

		public enPlayType[,] wBoard = new enPlayType[4, 4] {{enPlayType.None,enPlayType.None,enPlayType.None,enPlayType.None},
															 {enPlayType.None,enPlayType.None,enPlayType.None,enPlayType.None},
															 {enPlayType.None,enPlayType.None,enPlayType.None,enPlayType.None},
															 {enPlayType.None,enPlayType.None,enPlayType.None,enPlayType.None}};

		// BitBlt for screen capture

		[DllImport("GDI32.dll")]
		public static extern bool BitBlt(IntPtr hdcDest,int nXDest,int nYDest,
			int nWidth,int nHeight,IntPtr hdcSrc,
			int nXSrc,int nYSrc,int dwRop);

		#endregion

		#region�Initializations 


		[STAThread]
		static void Main() 
		{
			Application.Run(new frmTicTacToe());
		}
	
		private void Form1_Load(object sender, System.EventArgs e)
		{

			// Configures status bar and game for one player vs CPU
		
			stbMessage.Panels.Add("");
			stbMessage.Panels[0].AutoSize=StatusBarPanelAutoSize.Spring;
			gbScore.Location=new Point(7,256);
			mnOnePlayer_Click(null,null);
			
		}

		#endregion

		#region�Game (2 players) 

		public void RestartGame()
		{

			// Restart the whole game

			picWinner.Visible=false;
			pic11.Image=null;pic12.Image=null;pic13.Image=null;
			pic21.Image=null;pic22.Image=null;pic23.Image=null;
			pic31.Image=null;pic32.Image=null;pic33.Image=null;

			wTurn=enPlayType.Ball;
			wWinner=enPlayType.None;
			SetStatusMessage("");

			wBoard = new enPlayType[4, 4] {{enPlayType.None,enPlayType.None,enPlayType.None,enPlayType.None},
																{enPlayType.None,enPlayType.None,enPlayType.None,enPlayType.None},
																{enPlayType.None,enPlayType.None,enPlayType.None,enPlayType.None},
																{enPlayType.None,enPlayType.None,enPlayType.None,enPlayType.None}};

			objGameAI=new GameAI(this);

		
			// Ball allways begin, if CPU is ball make the computer move

			if ((wTypeCPU==enPlayType.Ball) && (wNumberPlayers==1))
				objGameAI.MakeComputerMove();

		}

		private void pic1_Click(object sender, System.EventArgs e)
		{

			// Gets the clicked picture and gets it�s row/column

			System.Windows.Forms.PictureBox wImage = (System.Windows.Forms.PictureBox)(sender);
			string wNumber=((System.Windows.Forms.Control)(((System.Windows.Forms.PictureBox)(sender)))).Name;
			int wRow =int.Parse(wNumber.Substring(3,1));
			int wColumn =int.Parse(wNumber.Substring(4,1));

			// Make the move using the row/column of the clicked picure

			MakeMove(wRow,wColumn);
		}

		public void MakeMove(int pRow, int pColumn)
		{
		
			int wRow=pRow;
			int wColumn=pColumn;

			if ((stbMessage.Panels[0].Text=="Connecting...") ||
				(stbMessage.Panels[0].Text=="Waiting for connection..."))
				return;

			// Gets the picture object to the changed

			System.Windows.Forms.PictureBox wImage=pic11;

			if (wRow==1 && wColumn==1) wImage=pic11;
			if (wRow==1 && wColumn==2) wImage=pic12;
			if (wRow==1 && wColumn==3) wImage=pic13;
			if (wRow==2 && wColumn==1) wImage=pic21;
			if (wRow==2 && wColumn==2) wImage=pic22;
			if (wRow==2 && wColumn==3) wImage=pic23;
			if (wRow==3 && wColumn==1) wImage=pic31;
			if (wRow==3 && wColumn==2) wImage=pic32;
			if (wRow==3 && wColumn==3) wImage=pic33;
			// Verify if the position is empty

			if (wBoard[wRow,wColumn]==enPlayType.None)
			{
				wBoard[wRow,wColumn]=wTurn;

				if (wTurn==enPlayType.Cross)
				{
					
					// Places a X
				
					wImage.Image=ilBolaXis.Images[0];
					wImage.Refresh();
					wTurn=enPlayType.Ball;
				}
				else
				{
					
					// Places an O
				
					wImage.Image=ilBolaXis.Images[1];
					wImage.Refresh();
					wTurn=enPlayType.Cross;
				}
			
				if (wWinner==enPlayType.None)
				{
				
					// Verify if someone has won

					VerifyWinner();

					// Move CPU move (if there is no winner and it is a one player game)
		
					if ((wTurn==wTypeCPU) && (wNumberPlayers==1))
						objGameAI.MakeComputerMove();	
				}

			}	

		}


		private void VerifyWinner()
		{
	
			// Function to check if someone has won

			int wSum1=(int)wBoard[1,1]+(int)wBoard[1,2]+(int)wBoard[1,3];
			int wSum2=(int)wBoard[2,1]+(int)wBoard[2,2]+(int)wBoard[2,3];
			int wSum3=(int)wBoard[3,1]+(int)wBoard[3,2]+(int)wBoard[3,3];

			int wSum4=(int)wBoard[1,1]+(int)wBoard[2,1]+(int)wBoard[3,1];
			int wSum5=(int)wBoard[1,2]+(int)wBoard[2,2]+(int)wBoard[3,2];
			int wSum6=(int)wBoard[1,3]+(int)wBoard[2,3]+(int)wBoard[3,3];

			int wSum7=(int)wBoard[1,1]+(int)wBoard[2,2]+(int)wBoard[3,3];
			int wSum8=(int)wBoard[3,1]+(int)wBoard[2,2]+(int)wBoard[1,3];

			// Show the winner
	
			if ((wSum1==3) || (wSum2==3) || (wSum3==3) ||
				(wSum4==3) || (wSum5==3) || (wSum6==3) ||
				(wSum7==3) || (wSum8==3))
			{
				wWinner=enPlayType.Ball;
				SetStatusMessage("The player BALL wins!");
				wScoreTotalMatches++;
				wScoreBall++;
				RefreshScore();
				
				goto winner;
			}

			if ((wSum1==30) || (wSum2==30) || (wSum3==30) || 
				(wSum4==30) || (wSum5==30) || (wSum6==30) ||
				(wSum7==30) || (wSum8==30))
			{
				wWinner=enPlayType.Cross;
				SetStatusMessage("The player CROSS wins!");
				wScoreTotalMatches++;
				wScoreCross++;
				RefreshScore();

				goto winner;
			}


			// Verify draw

			bool wDraw=true;
			int e=0;
			int f=0;

			for (e=1;e<=3;e++)
				for (f=1;f<=3;f++)
					if (wBoard[f,e]==enPlayType.None)
					{
						wDraw=false;
						goto winner;
					}
			
			if (wDraw==true)
			{
				SetStatusMessage("DRAW!");
				DrawLine(0,enLineType.Vertical);

				wScoreTotalMatches++;
				wScoreDraw++;
				RefreshScore();

				return;
			}

			winner:


			// Draw a line where there is 3 of the same kind

			if ((wSum1==3) || (wSum1==30))
				DrawLine(1,enLineType.Horizontal);
			if ((wSum2==3) || (wSum2==30))
				DrawLine(2,enLineType.Horizontal);
			if ((wSum3==3) || (wSum3==30))
				DrawLine(3,enLineType.Horizontal);
		
			if ((wSum4==3) || (wSum4==30))
				DrawLine(1,enLineType.Vertical);
			if ((wSum5==3) || (wSum5==30))
				DrawLine(2,enLineType.Vertical);
			if ((wSum6==3) || (wSum6==30))
				DrawLine(3,enLineType.Vertical);

			if ((wSum7==3) || (wSum7==30))
				DrawLine(1,enLineType.DiagonalRight);
			
			if ((wSum8==3) || (wSum8==30))
				DrawLine(1,enLineType.DiagonalLeft);
			
		}
		
		private void DrawLine(int pPosition, enLineType pLineType)
		{

			// Saves position and line type in globals for the 
			// OnPaint routine of picWinner draw the line
	
			wLinePosition=pPosition;
			wLineType=pLineType;

	
			// Captures game screen (BitBlt)
	
			Graphics g1  = this.CreateGraphics();
			Bitmap MyImage = new Bitmap(this.ClientRectangle.Width, this.ClientRectangle.Height, g1);
			Graphics g2   = Graphics.FromImage(MyImage);
			IntPtr dc1   = g1.GetHdc();
			IntPtr dc2  = g2.GetHdc();
			BitBlt(dc2, 0, 0, this.ClientRectangle.Width, this.ClientRectangle.Height, dc1, 0, 0, 13369376);
			g1.ReleaseHdc(dc1);
			g2.ReleaseHdc(dc2);
		
			// Puts game screen in picWinner
		
			picWinner.Image=MyImage;

		
			// Show the picure using Invoke because of socket thread 
			// (locks the game if you set picWinner.Visible=True)
	
			object[] p = new object[1];
			p[0] = picWinner;
		    picWinner.Invoke(new MakeVisibleHandler(MakeVisible), p);

		}

		public delegate void MakeVisibleHandler(Control control);

		public void MakeVisible(Control control)
		{
		
			// Make changes to UI using Invoke because of socket thread
		
			control.Visible = true;
		}

		private void picWinner_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{

			
			// Draw winner line in picWinner
		
			System.Drawing.Pen myPen=new System.Drawing.Pen(System.Drawing.Color.RoyalBlue,6);

			if (wWinner==enPlayType.Ball)
				myPen.Color=Color.RoyalBlue;

			if (wWinner==enPlayType.Cross)
				myPen.Color=Color.OrangeRed;

			switch(wLineType)
			{
				case enLineType.Horizontal:
				{
					e.Graphics.DrawLine(myPen, 25, 55+(wLinePosition-1)*73, picWinner.Height-10, 55+(wLinePosition-1)*73);
					break;
				}
				case enLineType.Vertical:
				{
					e.Graphics.DrawLine(myPen, 65+(wLinePosition-1)*75, 25, 65+(wLinePosition-1)*75,picWinner.Width-50);
					break;
				}
				case enLineType.DiagonalRight:
				{
					e.Graphics.DrawLine(myPen, 25, 25,  picWinner.Height-10,picWinner.Width-65);
					break;
				}
				case enLineType.DiagonalLeft:
				{
					e.Graphics.DrawLine(myPen, picWinner.Height-10, 25,  25,picWinner.Width-55);
					break;
				}
			}

			myPen.Dispose();
			
		}


		private void RefreshScore()
		{
			
			// Refreshes the score (Matches, Ball, Cross, Draws)
			
			lbMaches.Text= wScoreTotalMatches.ToString() + " Maches";
			lbScoreBall.Text= wScoreBall.ToString();
			lbScoreCross.Text= wScoreCross.ToString();
			lbScoreDraw.Text= wScoreDraw.ToString();
		}

		public void SetStatusMessage(string pMensagem)
		{
			
			// Set status bar message
		
			stbMessage.Panels[0].Text=pMensagem;
		}

		#endregion
	
		#region�Menu 


		private void mnExit_Click(object sender, System.EventArgs e)
		{
			
			// Exit the game
		
			Application.Exit();
		}

		private void mnResetScore_Click(object sender, System.EventArgs e)
		{
			
			// Reset score
		
			wScoreTotalMatches=0;
			wScoreBall=0;
			wScoreCross=0;
			wScoreDraw=0;
		
			RefreshScore();
		}

		private void mnOnePlayer_Click(object sender, System.EventArgs e)
		{

		
			// Sets game for one player

			wNumberPlayers=1;
			mnTwoPlayers.Checked=false;
			mnOnePlayer.Checked=true;

			mnDificuldade.Enabled=true;
			mnCPUO.Enabled=true;
			mnCPUX.Enabled=true;
			mnCPUX_Click(null,null);

			RestartGame();

		}

		private void mnTwoPlayers_Click(object sender, System.EventArgs e)
		{
			
			// Sets game for two players
			
			wNumberPlayers=2;
			mnOnePlayer.Checked=false;
			mnTwoPlayers.Checked=true;

			mnDificuldade.Enabled=false;
			mnCPUO.Enabled=false;
			mnCPUX.Enabled=false;

			RestartGame();
		
		}

		private void mnCPUX_Click(object sender, System.EventArgs e)
		{
		
			
			// Sets CPU to play as CROSS (X)
		
			wTypeCPU=enPlayType.Cross;
			wTypePlayer=enPlayType.Ball;
			mnCPUO.Checked=false;
			mnCPUX.Checked=true;
			
			RestartGame();

		}

		private void mnCPUO_Click(object sender, System.EventArgs e)
		{
		
	
			// Sets CPU to play as BALL (O)

			wTypeCPU=enPlayType.Ball;
			wTypePlayer=enPlayType.Cross;
			mnCPUX.Checked=false;
			mnCPUO.Checked=true;

			RestartGame();
		}

		private void mnDifficultHard_Click(object sender, System.EventArgs e)
		{
		

			// Sets hard difficulty for one player

			wDifficulty=enDifficulty.Hard;
			mnDifficultEasy.Checked=false;
			mnDifficultAverage.Checked=false;
			mnDifficultHard.Checked=true;
			
			RestartGame();
		}

		private void mnDifficultAverage_Click(object sender, System.EventArgs e)
		{
		

			// Sets average difficulty for one player

			wDifficulty=enDifficulty.Average;
			mnDifficultEasy.Checked=false;
			mnDifficultAverage.Checked=true;
			mnDifficultHard.Checked=false;
			
			RestartGame();
		}

		private void mnDifficultEasy_Click(object sender, System.EventArgs e)
		{
		

			// Sets easy difficulty for one player

			mnDifficultEasy.Checked=true;
			mnDifficultAverage.Checked=false;
			mnDifficultHard.Checked=false;
			
			wDifficulty=enDifficulty.Easy;
			RestartGame();
		}

	
		#endregion
		
		#region�Keyboard control 
 
		public void frmTicTacToe_KeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
		{

			// Keybord control (123,465,789) (ZXC,ASD,QWE)
			
			bool wNumeric=false;

			if (wNumberPlayers==2)
			{

				if (wControl==enControl.BallRight && wTurn==enPlayType.Ball)
					wNumeric=true;

				if (wControl==enControl.BallLeft && wTurn==enPlayType.Cross)
					wNumeric=true;

				if (wNumeric==true)
				{

					// Control via NumPAD
					
					if (e.KeyCode==Keys.NumPad7)
						MakeMove(1,1);
			
					if (e.KeyCode==Keys.NumPad8)
						MakeMove(1,2);

					if (e.KeyCode==Keys.NumPad9)
						MakeMove(1,3);
				
					if (e.KeyCode==Keys.NumPad4)
						MakeMove(2,1);
					
					if (e.KeyCode==Keys.NumPad5)
						MakeMove(2,2);

					if (e.KeyCode==Keys.NumPad6)
						MakeMove(2,3);

					if (e.KeyCode==Keys.NumPad1)
						MakeMove(3,1);
				
					if (e.KeyCode==Keys.NumPad2)
						MakeMove(3,2);

					if (e.KeyCode==Keys.NumPad3)
						MakeMove(3,3);
				}
				else
				{
					
					// Control via QWE
				
					if (e.KeyCode==Keys.Q)
						MakeMove(1,1);
			
					if (e.KeyCode==Keys.W)
						MakeMove(1,2);

					if (e.KeyCode==Keys.E)
						MakeMove(1,3);
				
					if (e.KeyCode==Keys.A)
						MakeMove(2,1);
					
					if (e.KeyCode==Keys.S)
						MakeMove(2,2);

					if (e.KeyCode==Keys.D)
						MakeMove(2,3);

					if (e.KeyCode==Keys.Z)
						MakeMove(3,1);
				
					if (e.KeyCode==Keys.X)
						MakeMove(3,2);

					if (e.KeyCode==Keys.C)
						MakeMove(3,3);
				}
			}
			else
			{
				
				// For one player, both sides of the keyboard work
			
				if (e.KeyCode==Keys.NumPad7 || e.KeyCode==Keys.Q)
					MakeMove(1,1);
			
				if (e.KeyCode==Keys.NumPad8 || e.KeyCode==Keys.W)
					MakeMove(1,2);

				if (e.KeyCode==Keys.NumPad9 || e.KeyCode==Keys.E)
					MakeMove(1,3);
				
				if (e.KeyCode==Keys.NumPad4 || e.KeyCode==Keys.A)
					MakeMove(2,1);
					
				if (e.KeyCode==Keys.NumPad5 || e.KeyCode==Keys.S)
					MakeMove(2,2);

				if (e.KeyCode==Keys.NumPad6 || e.KeyCode==Keys.D)
					MakeMove(2,3);

				if (e.KeyCode==Keys.NumPad1 || e.KeyCode==Keys.Z)
					MakeMove(3,1);
				
				if (e.KeyCode==Keys.NumPad2 || e.KeyCode==Keys.X)
					MakeMove(3,2);

				if (e.KeyCode==Keys.NumPad3 || e.KeyCode==Keys.C)
					MakeMove(3,3);
			}

		}

		#endregion
	}
}

