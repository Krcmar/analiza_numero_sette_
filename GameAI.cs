using System;

namespace TicTacToe
{

	public class GameAI
	{
		
		private enPlayType wTypeCPU;
		private enPlayType wTypePlayer;
		private enDifficulty wDifficulty;
		frmTicTacToe objTicTacToe;

		#region�Constructor 

		public GameAI(frmTicTacToe objForm)
		{
		
			// Gets main form reference, who is CPU, who is player and
			// difficulty

			objTicTacToe=objForm;
			wTypeCPU=objTicTacToe.wTypeCPU;
			wTypePlayer=objTicTacToe.wTypePlayer;
			wDifficulty=objTicTacToe.wDifficulty;

		}


		#endregion

		#region�Game (1 player) 

		public void MakeComputerMove()
		{


			// CPU plays

			bool wNothing=false;

			// Return if it is not CPU turn

			if (objTicTacToe.wTurn==wTypePlayer)
				return;

			// Routine to check if it is possible to win the game
			// Return if a move was made

			PlayWinner(false,ref wNothing);

			if (objTicTacToe.wTurn==wTypePlayer)
				return;


			// Routine to check if there is a move that prevents the player to win
			// Return if a move was made

			PlayDefensive();

			if (objTicTacToe.wTurn==wTypePlayer)
				return;


			// Routine to make and avoid "traps"

			PlayOffensive();

		}
	
		private void PlayWinner(bool pJustVerifyMove,ref bool rGoodMove)
		{

			// Routine to check if it is possible to win the game (sums lines and check totals latter)

			if (wDifficulty==enDifficulty.Average)
			{
				rGoodMove=true;
				return;
			}

			int wSum1=(int)objTicTacToe.wBoard[1,1]+(int)objTicTacToe.wBoard[1,2]+(int)objTicTacToe.wBoard[1,3];
			int wSum2=(int)objTicTacToe.wBoard[2,1]+(int)objTicTacToe.wBoard[2,2]+(int)objTicTacToe.wBoard[2,3];
			int wSum3=(int)objTicTacToe.wBoard[3,1]+(int)objTicTacToe.wBoard[3,2]+(int)objTicTacToe.wBoard[3,3];

			int wSum4=(int)objTicTacToe.wBoard[1,1]+(int)objTicTacToe.wBoard[2,1]+(int)objTicTacToe.wBoard[3,1];
			int wSum5=(int)objTicTacToe.wBoard[1,2]+(int)objTicTacToe.wBoard[2,2]+(int)objTicTacToe.wBoard[3,2];
			int wSum6=(int)objTicTacToe.wBoard[1,3]+(int)objTicTacToe.wBoard[2,3]+(int)objTicTacToe.wBoard[3,3];

			int wSum7=(int)objTicTacToe.wBoard[1,1]+(int)objTicTacToe.wBoard[2,2]+(int)objTicTacToe.wBoard[3,3];
			int wSum8=(int)objTicTacToe.wBoard[3,1]+(int)objTicTacToe.wBoard[2,2]+(int)objTicTacToe.wBoard[1,3];


			// Calculates sum that indicates a good move for CPU 

			int wCPUSum=0;

			if (wTypeCPU==enPlayType.Cross)
				wCPUSum=20;
			else
				wCPUSum=2;

			// If sum checks, plays in the available position to win the game

			if (wSum1==wCPUSum)
			{
				if (objTicTacToe.wBoard[1,1]==enPlayType.None)
				{
					if (pJustVerifyMove==false)
						objTicTacToe.MakeMove(1,1);
					rGoodMove=true;
					return;
				}
				if (objTicTacToe.wBoard[1,2]==enPlayType.None)
				{
					if (pJustVerifyMove==false)
						objTicTacToe.MakeMove(1,2);
					rGoodMove=true;
					return;
				}
				if (objTicTacToe.wBoard[1,3]==enPlayType.None)
				{
					if (pJustVerifyMove==false)
						objTicTacToe.MakeMove(1,3);
					rGoodMove=true;
					return;
				}
			}

			if (wSum2==wCPUSum)
			{
				if (objTicTacToe.wBoard[2,1]==enPlayType.None)
				{
					if (pJustVerifyMove==false)
						objTicTacToe.MakeMove(2,1);
					rGoodMove=true;
					return;
				}
				if (objTicTacToe.wBoard[2,2]==enPlayType.None)
				{
					if (pJustVerifyMove==false)
						objTicTacToe.MakeMove(2,2);
					rGoodMove=true;
					return;
				}
				if (objTicTacToe.wBoard[2,3]==enPlayType.None)
				{
					if (pJustVerifyMove==false)
						objTicTacToe.MakeMove(2,3);
					rGoodMove=true;
					return;
				}
			}

			if (wSum3==wCPUSum)
			{
				if (objTicTacToe.wBoard[3,1]==enPlayType.None)
				{
					if (pJustVerifyMove==false)
						objTicTacToe.MakeMove(3,1);
					rGoodMove=true;
					return;
				}
				if (objTicTacToe.wBoard[3,2]==enPlayType.None)
				{
					if (pJustVerifyMove==false)
						objTicTacToe.MakeMove(3,2);
					rGoodMove=true;
					return;
				}
				if (objTicTacToe.wBoard[3,3]==enPlayType.None)
				{
					if (pJustVerifyMove==false)
						objTicTacToe.MakeMove(3,3);
					rGoodMove=true;
					return;
				}
			}
			
			if (wSum4==wCPUSum)
			{
				if (objTicTacToe.wBoard[1,1]==enPlayType.None)
				{
					if (pJustVerifyMove==false)
						objTicTacToe.MakeMove(1,1);
					rGoodMove=true;
					return;
				}
				if (objTicTacToe.wBoard[2,1]==enPlayType.None)
				{
					if (pJustVerifyMove==false)
						objTicTacToe.MakeMove(2,1);
					rGoodMove=true;
					return;
				}
				if (objTicTacToe.wBoard[3,1]==enPlayType.None)
				{
					if (pJustVerifyMove==false)
						objTicTacToe.MakeMove(3,1);
					rGoodMove=true;
					return;
				}
			}

			if (wSum5==wCPUSum)
			{
				if (objTicTacToe.wBoard[1,2]==enPlayType.None)
				{
					if (pJustVerifyMove==false)
						objTicTacToe.MakeMove(1,2);
					rGoodMove=true;
					return;
				}
				if (objTicTacToe.wBoard[2,2]==enPlayType.None)
				{
					if (pJustVerifyMove==false)
						objTicTacToe.MakeMove(2,2);
					rGoodMove=true;
					return;
				}
				if (objTicTacToe.wBoard[3,2]==enPlayType.None)
				{
					if (pJustVerifyMove==false)
						objTicTacToe.MakeMove(3,2);
					rGoodMove=true;
					return;
				}
			}

			if (wSum6==wCPUSum)
			{
				if (objTicTacToe.wBoard[1,3]==enPlayType.None)
				{
					if (pJustVerifyMove==false)
						objTicTacToe.MakeMove(1,3);
					rGoodMove=true;
					return;
				}
				if (objTicTacToe.wBoard[2,3]==enPlayType.None)
				{
					if (pJustVerifyMove==false)
						objTicTacToe.MakeMove(2,3);
					rGoodMove=true;
					return;
				}
				if (objTicTacToe.wBoard[3,3]==enPlayType.None)
				{
					if (pJustVerifyMove==false)
						objTicTacToe.MakeMove(3,3);
					rGoodMove=true;
					return;
				}
			}

			if (wSum7==wCPUSum)
			{
				if (objTicTacToe.wBoard[1,1]==enPlayType.None)
				{
					if (pJustVerifyMove==false)
						objTicTacToe.MakeMove(1,1);
					rGoodMove=true;
					return;
				}
				if (objTicTacToe.wBoard[2,2]==enPlayType.None)
				{
					if (pJustVerifyMove==false)
						objTicTacToe.MakeMove(2,2);
					rGoodMove=true;
					return;
				}
				if (objTicTacToe.wBoard[3,3]==enPlayType.None)
				{
					if (pJustVerifyMove==false)
						objTicTacToe.MakeMove(3,3);
					rGoodMove=true;
					return;
				}
			}

			if (wSum8==wCPUSum)
			{
				if (objTicTacToe.wBoard[3,1]==enPlayType.None)
				{
					if (pJustVerifyMove==false)
						objTicTacToe.MakeMove(3,1);
					rGoodMove=true;
					return;
				}
				if (objTicTacToe.wBoard[2,2]==enPlayType.None)
				{
					if (pJustVerifyMove==false)
						objTicTacToe.MakeMove(2,2);
					rGoodMove=true;
					return;
				}
				if (objTicTacToe.wBoard[1,3]==enPlayType.None)
				{
					if (pJustVerifyMove==false)
						objTicTacToe.MakeMove(1,3);
					rGoodMove=true;
					return;
				}
			}

		}

		private void PlayDefensive()
		{
		    // Routine to check if there is a move that prevents the player to win(sum lines and check totals latter)

			if (wDifficulty==enDifficulty.Average)		
			{
				System.Threading.Thread.Sleep(15);
				System.Random objRandom=new Random();
				int rnd=objRandom.Next(1,6);
				if (rnd==3)
					return;
			}

			if (wDifficulty==enDifficulty.Easy)		
			{
				System.Threading.Thread.Sleep(15);
				System.Random objRandom=new Random();
				int rnd=objRandom.Next(1,3);
				if (rnd==1)
					return;
			}

			// Sums lines and check totals latter

			int wSum1=(int)objTicTacToe.wBoard[1,1]+(int)objTicTacToe.wBoard[1,2]+(int)objTicTacToe.wBoard[1,3];
			int wSum2=(int)objTicTacToe.wBoard[2,1]+(int)objTicTacToe.wBoard[2,2]+(int)objTicTacToe.wBoard[2,3];
			int wSum3=(int)objTicTacToe.wBoard[3,1]+(int)objTicTacToe.wBoard[3,2]+(int)objTicTacToe.wBoard[3,3];

			int wSum4=(int)objTicTacToe.wBoard[1,1]+(int)objTicTacToe.wBoard[2,1]+(int)objTicTacToe.wBoard[3,1];
			int wSum5=(int)objTicTacToe.wBoard[1,2]+(int)objTicTacToe.wBoard[2,2]+(int)objTicTacToe.wBoard[3,2];
			int wSum6=(int)objTicTacToe.wBoard[1,3]+(int)objTicTacToe.wBoard[2,3]+(int)objTicTacToe.wBoard[3,3];

			int wSum7=(int)objTicTacToe.wBoard[1,1]+(int)objTicTacToe.wBoard[2,2]+(int)objTicTacToe.wBoard[3,3];
			int wSum8=(int)objTicTacToe.wBoard[3,1]+(int)objTicTacToe.wBoard[2,2]+(int)objTicTacToe.wBoard[1,3];

			//Calculates sum that indicates two piecies in a row for human player
			
			int wPlayerSum=0;

			if (wTypePlayer==enPlayType.Cross)
				wPlayerSum=20;
			else
				wPlayerSum=2;

			// If sum checks, plays in the available position preventing player to win

			if (wSum1==wPlayerSum)
			{
				if (objTicTacToe.wBoard[1,1]==enPlayType.None) {
					objTicTacToe.MakeMove(1,1);return;}
				if (objTicTacToe.wBoard[1,2]==enPlayType.None) {
					objTicTacToe.MakeMove(1,2);return;}
				if (objTicTacToe.wBoard[1,3]==enPlayType.None) {
					objTicTacToe.MakeMove(1,3);return;}
			}

			if (wSum2==wPlayerSum)
			{
				if (objTicTacToe.wBoard[2,1]==enPlayType.None) {
					objTicTacToe.MakeMove(2,1);return;}
				if (objTicTacToe.wBoard[2,2]==enPlayType.None) {
					objTicTacToe.MakeMove(2,2);return;}
				if (objTicTacToe.wBoard[2,3]==enPlayType.None) {
					objTicTacToe.MakeMove(2,3);return;}
			}

			if (wSum3==wPlayerSum)
			{
				if (objTicTacToe.wBoard[3,1]==enPlayType.None) {
					objTicTacToe.MakeMove(3,1);return;}
				if (objTicTacToe.wBoard[3,2]==enPlayType.None) {
					objTicTacToe.MakeMove(3,2);return;}
				if (objTicTacToe.wBoard[3,3]==enPlayType.None) {
					objTicTacToe.MakeMove(3,3);return;}
			}
			
			if (wSum4==wPlayerSum)
			{
				if (objTicTacToe.wBoard[1,1]==enPlayType.None) {
					objTicTacToe.MakeMove(1,1);return;}
				if (objTicTacToe.wBoard[2,1]==enPlayType.None) {
					objTicTacToe.MakeMove(2,1);return;}
				if (objTicTacToe.wBoard[3,1]==enPlayType.None) {
					objTicTacToe.MakeMove(3,1);return;}
			}

			if (wSum5==wPlayerSum)
			{
				if (objTicTacToe.wBoard[1,2]==enPlayType.None) {
					objTicTacToe.MakeMove(1,2);return;}
				if (objTicTacToe.wBoard[2,2]==enPlayType.None) {
					objTicTacToe.MakeMove(2,2);return;}
				if (objTicTacToe.wBoard[3,2]==enPlayType.None) {
					objTicTacToe.MakeMove(3,2);return;}
			}

			if (wSum6==wPlayerSum)
			{
				if (objTicTacToe.wBoard[1,3]==enPlayType.None) {
					objTicTacToe.MakeMove(1,3);return;}
				if (objTicTacToe.wBoard[2,3]==enPlayType.None) {
					objTicTacToe.MakeMove(2,3);return;}
				if (objTicTacToe.wBoard[3,3]==enPlayType.None) {
					objTicTacToe.MakeMove(3,3);return;}
			}

			if (wSum7==wPlayerSum)
			{
				if (objTicTacToe.wBoard[1,1]==enPlayType.None) {
					objTicTacToe.MakeMove(1,1);return;}
				if (objTicTacToe.wBoard[2,2]==enPlayType.None) {
					objTicTacToe.MakeMove(2,2);return;}
				if (objTicTacToe.wBoard[3,3]==enPlayType.None) {
					objTicTacToe.MakeMove(3,3);return;}
			}

			if (wSum8==wPlayerSum)
			{
				if (objTicTacToe.wBoard[3,1]==enPlayType.None) {
					objTicTacToe.MakeMove(3,1);return;}
				if (objTicTacToe.wBoard[2,2]==enPlayType.None) {
					objTicTacToe.MakeMove(2,2);return;}
				if (objTicTacToe.wBoard[1,3]==enPlayType.None) {
					objTicTacToe.MakeMove(1,3);return;}
			}
			
		}

		private void PlayOffensive()
		{

			// Routine to make and avoid "traps"


			// Logic:
			//		- If player is O try in the middle
			//		- If player is X try playing in the inverse diagonal of the player
			//		- If there is a player�s piece in a corner try next to player�s piece (HARD difficulty)
			//		- Try smart moves to avoid "traps" (HARD difficulty)
			//		- Try on the corner  (checks in a random order if there is an available position)
			//		- Try on the sides
			//		- Try in the middle

			bool wGoodMove=false;

			if (wDifficulty==enDifficulty.Hard)
			{
				if (wTypePlayer==enPlayType.Cross)
				{
				
				
					//	If player is X try playing in the inverse diagonal of the player
					//	in case there is a player�s piece on the corner
				
					if ((objTicTacToe.wBoard[1,1]==enPlayType.None) && (objTicTacToe.wBoard[3,3]==wTypePlayer))
					{
						objTicTacToe.MakeMove(1,1);
						return;
					}
									
					if ((objTicTacToe.wBoard[1,3]==enPlayType.None) && (objTicTacToe.wBoard[3,1]==wTypePlayer))
					{
						objTicTacToe.MakeMove(1,3);
						return;
					}
					
					if ((objTicTacToe.wBoard[3,1]==enPlayType.None) && (objTicTacToe.wBoard[1,3]==wTypePlayer))
					{
						objTicTacToe.MakeMove(3,1);
						return;
					}
					
					if ((objTicTacToe.wBoard[3,3]==enPlayType.None) && (objTicTacToe.wBoard[1,1]==wTypePlayer))
					{
						objTicTacToe.MakeMove(3,3);
						return;
					}
				}
				else
				{
					
					// Try in the middle
					
					if (objTicTacToe.wBoard[2,2]==enPlayType.None)
					{
						objTicTacToe.MakeMove(2,2);
						return;
					}
				}
			}
			

			// If there is a player�s piece in a corner  try next to player�s piece (HARD difficulty)
			// Try smart moves to avoid "traps" (HARD difficulty)
	
			if (wDifficulty==enDifficulty.Hard)
			{
				

				// Moves:		 |M|O		 | |O 
				//				-----		-----
				//				 |X|		 |X|  
				//				-----		-----
				//				O| | 		O|M|
				//
				//
				//				O|M|		O| |   
				//				-----		-----
				//				 |X|		 |X| 
				//				-----		-----
				//				 | |O		 |M|O

				if ((objTicTacToe.wBoard[1,2]==enPlayType.None) && ((objTicTacToe.wBoard[1,3]==wTypePlayer) && (objTicTacToe.wBoard[3,1]==wTypePlayer)))
				{
					objTicTacToe.wBoard[1,2]=wTypeCPU;
					PlayWinner(true,ref wGoodMove);
					objTicTacToe.wBoard[1,2]=enPlayType.None;

					if (wGoodMove==true)
					{
						objTicTacToe.MakeMove(1,2);
						return;
					}
				}

				if ((objTicTacToe.wBoard[3,2]==enPlayType.None) && ((objTicTacToe.wBoard[1,3]==wTypePlayer) && (objTicTacToe.wBoard[3,1]==wTypePlayer)))
				{
					objTicTacToe.wBoard[3,2]=wTypeCPU;
					PlayWinner(true,ref wGoodMove);
					objTicTacToe.wBoard[3,2]=enPlayType.None;

					if (wGoodMove==true)
					{
						objTicTacToe.MakeMove(3,2);
						return;
					}
				}

				if ((objTicTacToe.wBoard[1,2]==enPlayType.None) && ((objTicTacToe.wBoard[1,1]==wTypePlayer) && (objTicTacToe.wBoard[3,3]==wTypePlayer)))
				{
					objTicTacToe.wBoard[1,2]=wTypeCPU;
					PlayWinner(true,ref wGoodMove);
					objTicTacToe.wBoard[1,2]=enPlayType.None;

					if (wGoodMove==true)
					{
						objTicTacToe.MakeMove(1,2);
						return;
					}
				}

				if ((objTicTacToe.wBoard[3,2]==enPlayType.None) && ((objTicTacToe.wBoard[1,1]==wTypePlayer) && (objTicTacToe.wBoard[3,3]==wTypePlayer)))
				{
					objTicTacToe.wBoard[3,2]=wTypeCPU;
					PlayWinner(true,ref wGoodMove);
					objTicTacToe.wBoard[3,2]=enPlayType.None;

					if (wGoodMove==true)
					{
						objTicTacToe.MakeMove(3,2);
						return;
					}
				}
				


				// Moves:		 |M|		O| | 
				//				-----		-----
				//				O|X|		M|X|  
				//				-----		-----
				//				 | |O		 |O| 
				//
				//
				//				 | |O		 | |  
				//				-----		-----
				//				 |X|M		 |X|O 
				//				-----		-----
				//				 |O|		O|M| 

				if ((objTicTacToe.wBoard[1,2]==enPlayType.None) && ((objTicTacToe.wBoard[2,1]==wTypePlayer) && (objTicTacToe.wBoard[3,3]==wTypePlayer)))
				{
					objTicTacToe.wBoard[1,2]=wTypeCPU;
					PlayWinner(true,ref wGoodMove);
					objTicTacToe.wBoard[1,2]=enPlayType.None;

					if (wGoodMove==true)
					{
						objTicTacToe.MakeMove(1,2);
						return;
					}
				}

				if ((objTicTacToe.wBoard[2,1]==enPlayType.None) && ((objTicTacToe.wBoard[1,1]==wTypePlayer) && (objTicTacToe.wBoard[3,2]==wTypePlayer)))
				{
					objTicTacToe.wBoard[2,1]=wTypeCPU;
					PlayWinner(true,ref wGoodMove);
					objTicTacToe.wBoard[2,1]=enPlayType.None;

					if (wGoodMove==true)
					{
						objTicTacToe.MakeMove(2,1);
						return;
					}
				}

				if ((objTicTacToe.wBoard[2,3]==enPlayType.None) && ((objTicTacToe.wBoard[1,3]==wTypePlayer) && (objTicTacToe.wBoard[3,2]==wTypePlayer)))
				{
					objTicTacToe.wBoard[2,3]=wTypeCPU;
					PlayWinner(true,ref wGoodMove);
					objTicTacToe.wBoard[2,3]=enPlayType.None;

					if (wGoodMove==true)
					{
						objTicTacToe.MakeMove(2,3);
						return;
					}
				}

				if ((objTicTacToe.wBoard[3,2]==enPlayType.None) && ((objTicTacToe.wBoard[2,3]==wTypePlayer) && (objTicTacToe.wBoard[3,1]==wTypePlayer)))
				{
					objTicTacToe.wBoard[3,2]=wTypeCPU;
					PlayWinner(true,ref wGoodMove);
					objTicTacToe.wBoard[3,2]=enPlayType.None;

					if (wGoodMove==true)
					{
						objTicTacToe.MakeMove(3,2);
						return;
					}
				}


				// Moves:		 |O|		O| | 
				//				-----		-----
				//				 |X|M		M|X|  
				//				-----		-----
				//				O| | 		 |O| 

				if ((objTicTacToe.wBoard[2,3]==enPlayType.None) && ((objTicTacToe.wBoard[3,1]==wTypePlayer) && (objTicTacToe.wBoard[1,2]==wTypePlayer)))
				{
					objTicTacToe.wBoard[2,3]=wTypeCPU;
					PlayWinner(true,ref wGoodMove);
					objTicTacToe.wBoard[2,3]=enPlayType.None;

					if (wGoodMove==true)
					{
						objTicTacToe.MakeMove(2,3);
						return;
					}
				}

				if ((objTicTacToe.wBoard[2,1]==enPlayType.None) && ((objTicTacToe.wBoard[1,1]==wTypePlayer) && (objTicTacToe.wBoard[3,2]==wTypePlayer)))
				{
					objTicTacToe.wBoard[2,1]=wTypeCPU;
					PlayWinner(true,ref wGoodMove);
					objTicTacToe.wBoard[2,1]=enPlayType.None;

					if (wGoodMove==true)
					{
						objTicTacToe.MakeMove(2,1);
						return;
					}
				}

				// Moves:		M|O|		 |O|M 
				//				-----		-----
				//				O|X|		 |X|O 
				//				-----		-----
				//				 | | 		 | |
				//
				//
				//				 | |		 | |   
				//				-----		-----
				//				 |X|O		O|X| 
				//				-----		-----
				//				 |O|M		M|O| 
				
				if ((objTicTacToe.wBoard[1,1]==enPlayType.None) && ((objTicTacToe.wBoard[1,2]==wTypePlayer) && (objTicTacToe.wBoard[2,1]==wTypePlayer)))
				{
					objTicTacToe.wBoard[1,1]=wTypeCPU;
					PlayWinner(true,ref wGoodMove);
					objTicTacToe.wBoard[1,1]=enPlayType.None;

					if (wGoodMove==true)
					{
						objTicTacToe.MakeMove(1,1);
						return;
					}
				}

				if ((objTicTacToe.wBoard[1,3]==enPlayType.None) && ((objTicTacToe.wBoard[1,2]==wTypePlayer) && (objTicTacToe.wBoard[2,3]==wTypePlayer)))
				{
					objTicTacToe.wBoard[1,3]=wTypeCPU;
					PlayWinner(true,ref wGoodMove);
					objTicTacToe.wBoard[1,3]=enPlayType.None;

					if (wGoodMove==true)
					{
						objTicTacToe.MakeMove(1,3);
						return;
					}
				}

				if ((objTicTacToe.wBoard[3,3]==enPlayType.None) && ((objTicTacToe.wBoard[2,3]==wTypePlayer) && (objTicTacToe.wBoard[3,2]==wTypePlayer)))
				{
					objTicTacToe.wBoard[3,3]=wTypeCPU;
					PlayWinner(true,ref wGoodMove);
					objTicTacToe.wBoard[3,3]=enPlayType.None;

					if (wGoodMove==true)
					{
						objTicTacToe.MakeMove(3,3);
						return;
					}
				}

				if ((objTicTacToe.wBoard[3,1]==enPlayType.None) && ((objTicTacToe.wBoard[2,1]==wTypePlayer) && (objTicTacToe.wBoard[3,2]==wTypePlayer)))
				{
					objTicTacToe.wBoard[3,1]=wTypeCPU;
					PlayWinner(true,ref wGoodMove);
					objTicTacToe.wBoard[3,1]=enPlayType.None;

					if (wGoodMove==true)
					{
						objTicTacToe.MakeMove(3,1);
						return;
					}
				}

			}


			//	Try on the corner

			System.Threading.Thread.Sleep(15);
			System.Random objRandom=new Random();
			int rnd=objRandom.Next(1,5);

			int[] wPosicaoPontaX = new int[5];
			int[] wPosicaoPontaY = new int[5];
		
			if (rnd==1)
			{
				wPosicaoPontaX[1]=1;wPosicaoPontaY[1]=1;
				wPosicaoPontaX[2]=1;wPosicaoPontaY[2]=3;
				wPosicaoPontaX[3]=3;wPosicaoPontaY[3]=1;
				wPosicaoPontaX[4]=3;wPosicaoPontaY[4]=3;
			}
			if (rnd==2)
			{
				
				wPosicaoPontaX[1]=1;wPosicaoPontaY[1]=3;
				wPosicaoPontaX[2]=3;wPosicaoPontaY[2]=1;
				wPosicaoPontaX[3]=3;wPosicaoPontaY[3]=3;
				wPosicaoPontaX[4]=1;wPosicaoPontaY[4]=1;
			}
			if (rnd==3)
			{
			
				wPosicaoPontaX[1]=3;wPosicaoPontaY[1]=1;
				wPosicaoPontaX[2]=3;wPosicaoPontaY[2]=3;
				wPosicaoPontaX[3]=1;wPosicaoPontaY[3]=1;
				wPosicaoPontaX[4]=1;wPosicaoPontaY[4]=3;
			}
			if (rnd==4)
			{
				
				wPosicaoPontaX[1]=3;wPosicaoPontaY[1]=3;
				wPosicaoPontaX[2]=1;wPosicaoPontaY[2]=1;
				wPosicaoPontaX[3]=1;wPosicaoPontaY[3]=3;
				wPosicaoPontaX[4]=3;wPosicaoPontaY[4]=1;
			}
			
			if (objTicTacToe.wBoard[wPosicaoPontaX[1],wPosicaoPontaY[1]]==enPlayType.None)
			{
				objTicTacToe.MakeMove(wPosicaoPontaX[1],wPosicaoPontaY[1]);
				return;
			}
		
			if (objTicTacToe.wBoard[wPosicaoPontaX[2],wPosicaoPontaY[2]]==enPlayType.None)
			{
				objTicTacToe.MakeMove(wPosicaoPontaX[2],wPosicaoPontaY[2]);
				return;
			}
		
			if (objTicTacToe.wBoard[wPosicaoPontaX[3],wPosicaoPontaY[3]]==enPlayType.None)
			{
				objTicTacToe.MakeMove(wPosicaoPontaX[3],wPosicaoPontaY[3]);
				return;
			}
		
			if (objTicTacToe.wBoard[wPosicaoPontaX[4],wPosicaoPontaY[4]]==enPlayType.None)
			{
				objTicTacToe.MakeMove(wPosicaoPontaX[4],wPosicaoPontaY[4]);
				return;
			}


			// Try on the sides

			if (objTicTacToe.wBoard[1,2]==enPlayType.None)
			{
				objTicTacToe.MakeMove(1,2);
				return;
			}

			if (objTicTacToe.wBoard[2,1]==enPlayType.None)
			{
				objTicTacToe.MakeMove(2,1);
				return;
			}

			if (objTicTacToe.wBoard[2,3]==enPlayType.None)
			{
				objTicTacToe.MakeMove(2,3);
				return;
			}

			if (objTicTacToe.wBoard[3,2]==enPlayType.None)
			{
				objTicTacToe.MakeMove(3,2);
				return;
			}


			// Try in the middle

			if (objTicTacToe.wBoard[2,2]==enPlayType.None)
			{
				objTicTacToe.MakeMove(2,2);
				return;
			}

		}

		#endregion

	}
}
