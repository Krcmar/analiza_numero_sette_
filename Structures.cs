using System;

namespace TicTacToe
{
	#region Data Structures 


	// Indicates the type of move

	public enum enPlayType
	{
		None=0,
		Ball=1,
		Cross=10
	}


	// Indicates CPU AI difficulty

	public enum enDifficulty
	{
		Easy=0,
		Average=1,
		Hard=2
	}


	// Indicates the type of line to draw in case of a winner

	public enum enLineType
	{
		Vertical=0,
		Horizontal=1,
		DiagonalRight=2,
		DiagonalLeft=3
	}

	#endregion
}
